55555 => int DEFAULT_SOUND_SERVER_PORT;

1000 => int BASE_ATTACK;
500 => int BASE_SUSTAIN;
1000 => int BASE_RELEASE;
8 => int BASE_JITTER;

48 => int MIN_NOTE;
72 => int MAX_NOTE;


// Current parameters.
int delay;
0 => int jitter;
int panning;
// Start with C4
60 => int currentNote;

int oscType;

// private
int frequency;

OscRecv OSCReceiver;
DEFAULT_SOUND_SERVER_PORT => OSCReceiver.port;
OSCReceiver.listen();

fun Osc getOscillator() {
    SinOsc sin;
    TriOsc tri;
    SawOsc saw;
    SqrOsc square;

    getFrequency() => float frequency;
    frequency => sin.freq;
    frequency => tri.freq;
    frequency => saw.freq;
    frequency => square.freq;

    [sin, tri, saw, square] @=> Osc oscillators[];

    Math.random2(0, oscillators.size() - 1) => int currentOscillator;

    <<< currentOscillator >>>;

    return oscillators[currentOscillator];
}

fun float getFrequency() {
    currentNote => Std.mtof => float freq;
    freq + (Math.randomf() - .5) * jitter => freq;
    <<< "Current frequency is", freq >>>;
    return freq;
}

fun void playNote(int energy)
{

    // Getting a random oscillator.
    getOscillator() => Osc s;

    s => ADSR e => ResonZ f => BiQuad filter => Gain out => Pan2 p => dac;

    // the gain is dependant on the energy.
    .4 + (.6 / energy) => s.gain;

    BASE_ATTACK * energy => float attack;
    BASE_SUSTAIN * energy => float sustain;
    BASE_RELEASE * energy => float release;

    // -1 left only, 0 center, 1 right only
    0 => panning;

    .1 => f.Q;

    // FIXME should reuse the params for the delay here.
    // TODO parameterize the third param! (what is it?)
    e.set( attack::ms, sustain::ms, .7, release::ms );
    panning => p.pan;

    e.keyOn();
    attack::ms => now;

    e.keyOff();
    release::ms => now;
}

fun void registerPlayEvent() {
    OSCReceiver.event( "/play, i i i i i" ) @=> OscEvent playEvent;
    while(true) {
        playEvent => now;
        while (playEvent.nextMsg() != 0) {
            playEvent.getInt() => int energy;
            playEvent.getInt() => int upEnergy;
            playEvent.getInt() => int downEnergy;
            playEvent.getInt() => int leftEnergy;
            playEvent.getInt() => int rightEnergy;
            <<< "receiving new play event", energy, upEnergy, downEnergy, leftEnergy, rightEnergy >>>;

            currentNote => int previousNote;

            upEnergy - downEnergy => int up;
            while (up > 1500) {
                currentNote + 1 => currentNote;
                up - 1000 => up;
            }
            if (currentNote > MAX_NOTE) {
                MAX_NOTE => currentNote;
            }

            downEnergy - upEnergy => int down;
            while (down > 1500) {
                currentNote - 1 => currentNote;
                down - 1000 => down;
            }
            if (currentNote < MIN_NOTE) {
                MIN_NOTE => currentNote;
            }

            if (currentNote == previousNote) {
                BASE_JITTER => jitter;
            } else {
                0 => jitter;
            }

            spork ~ playNote(energy);

            rightEnergy - leftEnergy => int right;
            while (right > 1500) {
                .75::second => now;
                 spork ~ playNote(energy);
                right - 1000 => right;
            }
        }
    }
}

registerPlayEvent();
