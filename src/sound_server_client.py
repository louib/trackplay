import os

from oscpy.client import OSCClient


DEFAULT_SOUND_SERVER_ADDRESS = 'localhost'
DEFAULT_SOUND_SERVER_PORT = 55555


def get_osc_client():
    sound_server_address = os.environ.get('SOUND_SERVER_ADDRESS') or DEFAULT_SOUND_SERVER_ADDRESS
    sound_server_port = os.environ.get('SOUND_SERVER_PORT') or DEFAULT_SOUND_SERVER_PORT

    return OSCClient(sound_server_address, sound_server_port)


def send_osc_ping_message(osc_client):
    osc_client.send_message(b'/play', [])


def send_osc_play_note_message(osc_client, *args):
    osc_client.send_message(b'/play', args)
