#!/usr/bin/python3
import threading
import os
import time
import struct
import math

from progress.bar import Bar


from sound_server_client import get_osc_client, send_osc_play_note_message

osc_client = get_osc_client()


# The struct definition is as follows:
# struct input_event {
#   struct timeval time;
#   unsigned short type;
#   unsigned short code;
#   unsigned int value;
# };
# For a total of 24 bytes
EVENT_STRUCT_DEF = 'llHHI'
EVENT_STRUCT_SIZE = struct.calcsize(EVENT_STRUCT_DEF)
# Relative positioning event.
EV_REL = 2
# Absolute positioning event.
EV_ABS = 3
# X positioning (absolute and relative)
ABS_X = REL_X = 0
# Y positioning (absolute and relative)
ABS_Y = REL_Y = 1

MAX_CYCLE_DURATION = 15
MAX_ENERGY = 300
energy = 0
up_energy = 0
down_energy = 0
left_energy = 0
right_energy = 0
energy_cycle_start = 0
energy_bar = None

# Lock used for increasing the energy from multiple threads.
energy_lock = threading.Lock()


UNSIGNED_INT_CUTOFF = 2**31
UNSIGNED_INT_MAX = 2**32

# Change in X,Y offset bigger than this will not
# be considered a continuous gesture.
ABSOLUTE_OFFSET_CUTOFF = 100


def recover_integer_sign(integer):
    '''
    This is a hack to recover the sign from the value,
    Which is originally unsigned but is translated as a signed
    value without proper conversion.
    '''
    if integer < UNSIGNED_INT_CUTOFF:
        return integer
    return math.copysign(UNSIGNED_INT_MAX - integer, -1)


class MouseInputThread(threading.Thread):
    def __init__(self, device_name, ):
        super(MouseInputThread, self).__init__()
        self.device_name = device_name
        self.quit = False
        self.last_x_value = None
        self.last_y_value = None

    def run(self):
        global energy
        global energy_bar
        global energy_cycle_start
        global up_energy
        global down_energy
        global left_energy
        global right_energy

        trackpad_input_file = open("/dev/input/" + device_name, "rb")
        while True:
            if self.quit:
                return

            trackpad_event = struct.unpack(EVENT_STRUCT_DEF, trackpad_input_file.read(EVENT_STRUCT_SIZE))
            seconds, msecs, type, code, value = trackpad_event

            # We only listen to absolute position and relative position events.
            # See https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/uapi/linux/input-event-codes.h
            # for details, but basically
            # define EV_REL 0x02
            # define EV_ABS 0x03
            if type != EV_REL and type != EV_ABS:
                continue

            # Ignoring codes that are not ABS_X, ABS_Y, REL_X and REL_Y
            if code != ABS_X and code != ABS_Y:
                continue

            with energy_lock:
                if energy == 0:
                    energy_cycle_start = int(time.time())

                if not energy_bar:
                    energy_bar = Bar('➕Gathering energy ➕', max=MAX_ENERGY)

                energy += 1
                energy_bar.next()

                if type == EV_REL:
                    if code == REL_X:
                        value = recover_integer_sign(value)
                        if value > 0:
                            right_energy += int(math.fabs(value))
                        elif value < 0:
                            left_energy += int(math.fabs(value))
                    elif code == REL_Y:
                        value = recover_integer_sign(value)
                        if value > 0:
                            up_energy += int(math.fabs(value))
                        elif value < 0:
                            down_energy += int(math.fabs(value))
                elif type == EV_ABS:
                    # Note that for the absolute positioning, point (0,0) is at the
                    # top-left of the trackpad.
                    if code == ABS_X:
                        if self.last_x_value is None:
                            self.last_x_value = value
                        elif abs(self.last_x_value - value) > ABSOLUTE_OFFSET_CUTOFF:
                            self.last_x_value = value
                        elif self.last_x_value > value:
                            left_energy += self.last_x_value - value
                            self.last_x_value = value
                        else:
                            right_energy += value - self.last_x_value
                            self.last_x_value = value
                    elif code == ABS_Y:
                        if self.last_y_value is None:
                            self.last_y_value = value
                        elif abs(self.last_y_value - value) > ABSOLUTE_OFFSET_CUTOFF:
                            self.last_y_value = value
                        elif self.last_y_value > value:
                            up_energy += self.last_y_value - value
                            self.last_y_value = value
                        else:
                            down_energy += value - self.last_y_value
                            self.last_y_value = value

                if energy >= MAX_ENERGY:
                    # This is the "power" of the note being played.
                    energy_cycle_duration = min(int(time.time()) - energy_cycle_start, MAX_CYCLE_DURATION)

                    # This is done here to make sure the display is legit.
                    energy_bar.finish()
                    energy_bar = None

                    print(
                        '(⏱️ {0})'.format(energy_cycle_duration),
                        '(⬆️ {0})'.format(up_energy),
                        '(⬇️ {0})'.format(down_energy),
                        '(➡️ {0})'.format(right_energy),
                        '(⬅️ {0})'.format(left_energy),
                    )

                    send_osc_play_note_message(
                        osc_client,
                        energy_cycle_duration,
                        up_energy,
                        down_energy,
                        left_energy,
                        right_energy
                    )

                    energy = 0
                    right_energy = 0
                    left_energy = 0
                    up_energy = 0
                    down_energy = 0
                    print(
                        'Maximum energy reached in {0} seconds. Playing a note 😺\n'.format(energy_cycle_duration),
                    )


mouse_input_threads = []
# Listening to all the mouse inputs.
device_names = list(filter(lambda device_name: device_name.startswith('event'), os.listdir('/dev/input/')))
print('Starting listening threads on', len(device_names), 'devices')
for device_name in device_names:
    mouse_input_thread = MouseInputThread(device_name)
    mouse_input_thread.start()
    mouse_input_threads.append(mouse_input_thread)


try:
    for mouse_input_thread in mouse_input_threads:
        mouse_input_thread.join()
except KeyboardInterrupt:
    print("Keyboard interrupted")
finally:
    for mouse_input_thread in mouse_input_threads:
        mouse_input_thread.quit = True
    for mouse_input_thread in mouse_input_threads:
        mouse_input_thread.join()
