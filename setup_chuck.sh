# Setup chuck
tmp_dir=$(mktemp -d -t chuck-XXXXXXXXXX)
cd $tmp_dir && wget https://github.com/ccrma/chuck/archive/chuck-1.3.5.2.zip
cd $tmp_dir && unzip chuck-1.3.5.2.zip
# cant run it with jack yet....
cd $tmp_dir/chuck-chuck-1.3.5.2/src && make linux-jack
cd $tmp_dir/chuck-chuck-1.3.5.2/src && make install
