# Trackplay

Indirect mapping of trackpad gestures for sound synthesis.

## Installation

Install those system dependencies (on debian based systems):
```
sudo apt-get install python3 python3-dev python3-virtualenv python3-setuptools python3-pip python3-wheel libjack0 libjack-dev
```

The current user will need to be part of the `input` group for the trackpad inputs to be accessible:
```
usermod -aG input $USER
```

Install the python libraries.
```
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```

The gesture tracker can now be started with:
```
python3 src/trackplay.py
```

And the ChucK sound server can be started with:
```
chuck src/sound_server.ck
```

## Readings

* https://chuck.cs.princeton.edu/doc/program/ugen.html
* https://www.music.mcgill.ca/~gary/307/week8/stk.html
* https://www.music.mcgill.ca/~gary/307/index.html#Outline
* https://ccrma.stanford.edu/software/stk/tutorial.html
* http://opensoundcontrol.org/spec-1_0
